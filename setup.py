# coding: utf-8

import os
from setuptools import find_packages

from distutils.core import setup
from src.alpha_gis import __version__


def read(filename):
    try:
        return open(os.path.join(os.path.dirname(__file__), filename)).read()
    except IOError:
        return ''

setup(
    name='alpha-gis',
    version=__version__,

    package_dir={'': 'src'},
    packages=find_packages('src'),
    include_package_data=True,

    url='https://bitbucket.org/tkretts/alpha-gis',
    license='',
    author='Anton Bogunkov',
    author_email='anton.bogunkov@gmail.com',
    description="BARS m3 extension for BARS.Alpha-BI integration",
    long_description=read('README.md'),
    install_requires=read('REQUIREMENTS.txt'),

    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Framework :: Django',
        'Environment :: Web Environment',
        'Natural Language :: Russian',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ]
)
