/**
 * Created by tkretts on 22.05.15.
 */

function showAllOnMap(){
    var alphaFrame = document.getElementById(
            '{{ component.alpha_iframe.client_id }}'),
        alphaUrl = '{{ component.alpha_api_url }}',
        // TODO: Может стоит сделать это как-нибудь по-другому
        authObj = {
            'login': '{{ component.alpha_login }}',
            'password': '{{ component.alpha_password }}'
        },
        allActionId = '{{ component.all_action_id }}';

    doAlphaAction(alphaUrl + 'login', authObj, function(alphaToken){
        alphaFrame.src = alphaUrl + '?OpenAction=' + allActionId +
            '&AuthToken=' + alphaToken;
    });
}
showAllOnMap();
