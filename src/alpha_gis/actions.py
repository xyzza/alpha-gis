# coding: utf-8
__author__ = 'tkretts'

from django.conf import settings
from django.utils.translation import ugettext as _

from m3.actions import Action, OperationResult
from m3_ext.ui.shortcuts import MessageBox
from m3_ext.ui.results import ExtUIScriptResult

from alpha_gis.ui import BaseAllPointsWindow


class AlphaActionPackMixin(object):
    """ Миксин для добавления паку возможности отображения ГИСов
    """
    alpha_config_name = None

    def __init__(self):
        super(AlphaActionPackMixin, self).__init__()
        if hasattr(self, 'actions'):
            self.alpha_refresh_action = AlphaRefreshAction()
            self.alpha_all_on_map_action = AlphaAllOnMapAction()
            self.actions.extend([
                self.alpha_refresh_action,
                self.alpha_all_on_map_action,
            ])

    @property
    def common_config(self):
        """ Общая конфигурация
            :rtype: dict
        """
        config = {
            'Login': getattr(settings, 'ALPHA_USER', ''),
            'Password': getattr(settings, 'ALPHA_PASS', ''),
        }
        return config

    @property
    def alpha_config(self):
        """ Конфигурация для конкретного приложения
            :rtype: dict
        """
        config = {}
        if self.alpha_config_name:
            config = getattr(settings, 'ALPHA_CONFIG', {}).get(
                self.alpha_config_name, {})

        return config

    @property
    def etl_config(self):
        """ Конфигурация для запуска ETL-процессов
        :rtype: dict
        """
        return self.alpha_config.get('REFRESH_CONFIG', {})


class AlphaAllOnMapAction(Action):
    """ Отображает на карте все помещения
    """
    url = r'/room-all-on-map$'

    @property
    def url(self):
        return r'/{0}-all-on-map$'.format(self.parent.alpha_config_name.lower())

    def context_declaration(self):
        return []

    def create_window(self):
        win = BaseAllPointsWindow()

        # TODO: Может стоит это сделать как-нибудь по-другому
        win.alpha_login = self.parent.common_config.get('Login')
        win.alpha_password = self.parent.common_config.get('Password')
        win.all_action_id = self.parent.alpha_config.get('ALL_ACTION_ID')

        return win

    def run(self, request, context):
        win = self.create_window()

        return ExtUIScriptResult(win)


class AlphaRefreshAction(Action):
    """ Экшн для обновления дополнительной информации по объектам ГИС
    """
    # FIXME: Вынести в templates
    handler_template = """
        doAlphaAction(
            '{alpha_url}login',
            {{
                Login: '{login}',
                Password: '{password}'
            }},
            function(alphaToken){{
                Ext.Ajax.request({{
                    url: '{alpha_url}Pipeline/Run?pipeLineId={pipeline}&' +
                            'logLevel=0&authToken=' + alphaToken,
                    method: 'GET'
                }});
            }}
        );
    """

    @property
    def url(self):
        return '/{0}_alpha_refresh'.format(
            self.parent.alpha_config_name.lower())

    def context_declaration(self):
        return []

    def run(self, request, context):
        confirm = request.POST.get("confirm")
        if confirm == "true":
            return OperationResult()

        msg_box = MessageBox(
            _(u"БАРС.Альфа-BI"),
            _(u"Вы действительно хотите обновить дополнительную информацию?"),
            MessageBox.ICON_QUESTION,
            MessageBox.BTN_YESNO)
        msg_box.handler_yes = self.handler_template.format(
            **{
                'alpha_url': getattr(settings, 'ALPHA_API_URL', ''),
                'login': self.parent.common_config.get('Login', ''),
                'password': self.parent.common_config.get('Password', ''),
                'pipeline': self.parent.etl_config.get('PIPELINE', ''),
            }
        )

        return ExtUIScriptResult(msg_box)
