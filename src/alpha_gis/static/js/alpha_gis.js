/**
 * Created by tkretts on 15.05.15.
 *
 * BARS.Alpha-BI GIS Integration
 *
 */


function obj2str(object){
    var str = "";
    for (var key in object) {
        if (str != "") {
            str += "&";
        }
        str += key + "=" + encodeURIComponent(object[key]);
    }
    return str;
}

function doAlphaAction(alphaUrl, authObj, callback){
    var token = Ext.util.Cookies.get('AlphaToken');

    authObj = Ext.applyIf(authObj, {
        AuthToken: token, ForceNewSession: false
    });

    Ext.Ajax.request({
        method: 'POST',
        url: alphaUrl,
        params: authObj,
        success: function(res) {
            var data = Ext.decode(res.responseText);
            if (data['Success'] === true) {
                token = data['TokenValue'];
                Ext.util.Cookies.set('AlphaToken', token);

                callback(token);
            } else {
                Ext.Msg.show({
                    title: 'Ошибка',
                    msg: data['Error'] + '\n' + 'Обратитесь к администратору',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
        }
    });
}